package com.company;

/**
 * Created by visitante on 24/07/2017.
 */


    class TestOuter2{
        static int data=30;
        static class Inner{
            static void msg(){System.out.println("El dato es "+data);}
        }
        public static void main(String args[]){
            TestOuter2.Inner.msg();//no se necesita crear la instacia de l static nested class
        }
    }

